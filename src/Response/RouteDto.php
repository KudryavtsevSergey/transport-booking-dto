<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

class RouteDto implements ResponseDtoInterface
{
    /**
     * @param int $id
     * @param string|null $name
     * @param RouteStopDto[] $stops
     */
    public function __construct(
        private int $id,
        private ?string $name,
        private array $stops,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getStops(): array
    {
        return $this->stops;
    }
}
