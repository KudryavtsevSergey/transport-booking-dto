<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

class RoutePriceCurrencyDto
{
    /**
     * @param int $currencyId
     * @param string $currencyIso
     * @param bool $isFromAnotherCurrency
     * @param int|null $fromCurrencyId
     * @param TicketPriceDto[] $ticketPrices
     */
    public function __construct(
        private int $currencyId,
        private string $currencyIso,
        private bool $isFromAnotherCurrency,
        private ?int $fromCurrencyId,
        private array $ticketPrices,
    ) {
    }

    public function getCurrencyId(): int
    {
        return $this->currencyId;
    }

    public function getCurrencyIso(): string
    {
        return $this->currencyIso;
    }

    public function getIsFromAnotherCurrency(): bool
    {
        return $this->isFromAnotherCurrency;
    }

    public function getFromCurrencyId(): ?int
    {
        return $this->fromCurrencyId;
    }

    public function getTicketPrices(): array
    {
        return $this->ticketPrices;
    }
}
