<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

class PageResponseDto implements ResponseDtoInterface
{
    public function __construct(
        private int $currentPage,
        private int $perPage,
        private int $total,
        private array $data,
    ) {
    }

    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    public function getPerPage(): int
    {
        return $this->perPage;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function getData(): array
    {
        return $this->data;
    }
}
