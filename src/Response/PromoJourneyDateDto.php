<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

class PromoJourneyDateDto
{
    public function __construct(
        private int $id,
        private int $journeyDateId,
        private ?int $countPromoCode,
        private int $holdPromoCodesCount,
        private int $ticketPromoCodesCount,
        private PromoDataDto $promoData,
        private PromoJourneyDto $promoJourney,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCountPromoCode(): ?int
    {
        return $this->countPromoCode;
    }

    public function getHoldPromoCodesCount(): int
    {
        return $this->holdPromoCodesCount;
    }

    public function getJourneyDateId(): int
    {
        return $this->journeyDateId;
    }

    public function getPromoData(): PromoDataDto
    {
        return $this->promoData;
    }

    public function getPromoJourney(): PromoJourneyDto
    {
        return $this->promoJourney;
    }

    public function getTicketPromoCodesCount(): int
    {
        return $this->ticketPromoCodesCount;
    }
}
