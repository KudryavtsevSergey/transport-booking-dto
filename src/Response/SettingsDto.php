<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

class SettingsDto implements ResponseDtoInterface
{
    public function __construct(
        private int $maximumNumberOfOneTypeBaggage,
        private int $maximumNumberOfOneTypeTickets,
    ) {
    }

    public function getMaximumNumberOfOneTypeBaggage(): int
    {
        return $this->maximumNumberOfOneTypeBaggage;
    }

    public function getMaximumNumberOfOneTypeTickets(): int
    {
        return $this->maximumNumberOfOneTypeTickets;
    }
}
