<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

class PaidServicePriceCurrencyDto
{
    public function __construct(
        private int $paidServicePriceId,
        private int $amount,
        private int $currencyId,
    ) {
    }

    public function getPaidServicePriceId(): int
    {
        return $this->paidServicePriceId;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getCurrencyId(): int
    {
        return $this->currencyId;
    }
}
