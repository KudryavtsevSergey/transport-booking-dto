<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

use Sun\TransportBookingDto\Response\Booking\StopDto;

class RouteWithStopsDto implements ResponseDtoInterface
{
    /**
     * @param RouteDto $route
     * @param StopDto[] $stops
     */
    public function __construct(
        private RouteDto $route,
        private array $stops,
    ) {
    }

    public function getRoute(): RouteDto
    {
        return $this->route;
    }

    public function getStops(): array
    {
        return $this->stops;
    }
}
