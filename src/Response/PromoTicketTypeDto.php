<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

class PromoTicketTypeDto
{
    public function __construct(
        private ?int $amountPercent,
        private bool $isAvailable,
        private int $ticketTypeId,
    ) {
    }

    public function getAmountPercent(): ?int
    {
        return $this->amountPercent;
    }

    public function getIsAvailable(): bool
    {
        return $this->isAvailable;
    }

    public function getTicketTypeId(): int
    {
        return $this->ticketTypeId;
    }
}
