<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

class PromoJourneyDto
{
    public function __construct(
        private int $journeyId,
        private int $promoDataId,
        private ?int $countPromoCode,
    ) {
    }

    public function getJourneyId(): int
    {
        return $this->journeyId;
    }

    public function getPromoDataId(): int
    {
        return $this->promoDataId;
    }

    public function getCountPromoCode(): int
    {
        return $this->countPromoCode;
    }
}
