<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

class DiscountDto
{
    public function __construct(
        private int $amount,
        private int $currencyId,
        private int $ticketTypeId,
    ) {
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getCurrencyId(): int
    {
        return $this->currencyId;
    }

    public function getTicketTypeId(): int
    {
        return $this->ticketTypeId;
    }
}
