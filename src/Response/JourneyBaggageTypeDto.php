<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

class JourneyBaggageTypeDto
{
    public function __construct(
        private int $journeyId,
        private int $baggageTypeId,
        private int $baggageTypePriceId,
        private ?string $baggageTypeName,
        private ?string $baggageTypeDescription,
    ) {
    }

    public function getJourneyId(): int
    {
        return $this->journeyId;
    }

    public function getBaggageTypeId(): int
    {
        return $this->baggageTypeId;
    }

    public function getBaggageTypePriceId(): int
    {
        return $this->baggageTypePriceId;
    }

    public function getBaggageTypeName(): ?string
    {
        return $this->baggageTypeName;
    }

    public function getBaggageTypeDescription(): ?string
    {
        return $this->baggageTypeDescription;
    }
}
