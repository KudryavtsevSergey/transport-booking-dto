<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

class TicketPriceDto
{
    public function __construct(
        private int $ticketTypeId,
        private ?string $ticketTypeName,
        private string $ticketTypeCode,
        private bool $isWithoutSeat,
        private int $amount,
    ) {
    }

    public function getTicketTypeId(): int
    {
        return $this->ticketTypeId;
    }

    public function getTicketTypeName(): ?string
    {
        return $this->ticketTypeName;
    }

    public function getTicketTypeCode(): string
    {
        return $this->ticketTypeCode;
    }

    public function getIsWithoutSeat(): bool
    {
        return $this->isWithoutSeat;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }
}
