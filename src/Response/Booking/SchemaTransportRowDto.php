<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking;

class SchemaTransportRowDto
{
    /**
     * @param int $id
     * @param int|null $name
     * @param int $transportId
     * @param SchemaTransportSeatDto[] $schemaTransportSeats
     */
    public function __construct(
        private int $id,
        private ?int $name,
        private int $transportId,
        private array $schemaTransportSeats,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): ?int
    {
        return $this->name;
    }

    public function getTransportId(): int
    {
        return $this->transportId;
    }

    public function getSchemaTransportSeats(): array
    {
        return $this->schemaTransportSeats;
    }
}
