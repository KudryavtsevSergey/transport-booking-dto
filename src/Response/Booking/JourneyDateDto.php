<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking;

use DateTimeInterface;
use Sun\TransportBookingDto\Response\ResponseDtoInterface;

class JourneyDateDto implements ResponseDtoInterface
{
    /**
     * @param DateTimeInterface[] $backDates
     * @param DateTimeInterface[] $forwardDates
     */
    public function __construct(
        private array $backDates = [],
        private array $forwardDates = [],
    ) {
    }

    public function getBackDates(): array
    {
        return $this->backDates;
    }

    public function getForwardDates(): array
    {
        return $this->forwardDates;
    }
}
