<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking;

use DateTimeInterface;

class JourneyDateStopDto
{
    public function __construct(
        private int $stopId,
        private ?string $stopName,
        private int $cityId,
        private ?string $cityName,
        private int $stopTypeId,
        private ?string $stopTypeName,
        private bool $stopTypeIsNeedTransfer,
        private string $timezone,
        private ?int $platformId,
        private ?string $platformName,
        private ?DateTimeInterface $arrivalAt,
        private ?DateTimeInterface $localArrivalAt,
        private ?DateTimeInterface $departureAt,
        private ?DateTimeInterface $localDepartureAt,
        private int $journeyStopId,
        private ?string $boardingStatus,
        private ?string $fullName,
    ) {
    }

    public function getStopId(): int
    {
        return $this->stopId;
    }

    public function getStopName(): ?string
    {
        return $this->stopName;
    }

    public function getCityId(): int
    {
        return $this->cityId;
    }

    public function getCityName(): ?string
    {
        return $this->cityName;
    }

    public function getStopTypeId(): int
    {
        return $this->stopTypeId;
    }

    public function getStopTypeName(): ?string
    {
        return $this->stopTypeName;
    }

    public function getStopTypeIsNeedTransfer(): bool
    {
        return $this->stopTypeIsNeedTransfer;
    }

    public function getTimezone(): string
    {
        return $this->timezone;
    }

    public function getPlatformId(): ?int
    {
        return $this->platformId;
    }

    public function getPlatformName(): ?string
    {
        return $this->platformName;
    }

    public function getArrivalAt(): ?DateTimeInterface
    {
        return $this->arrivalAt;
    }

    public function getLocalArrivalAt(): ?DateTimeInterface
    {
        return $this->localArrivalAt;
    }

    public function getDepartureAt(): ?DateTimeInterface
    {
        return $this->departureAt;
    }

    public function getLocalDepartureAt(): ?DateTimeInterface
    {
        return $this->localDepartureAt;
    }

    public function getJourneyStopId(): int
    {
        return $this->journeyStopId;
    }

    public function getBoardingStatus(): ?string
    {
        return $this->boardingStatus;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }
}
