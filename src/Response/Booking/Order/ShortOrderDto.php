<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking\Order;

use DateTimeInterface;
use Sun\TransportBookingDto\Response\ResponseDtoInterface;
use Sun\TransportBookingDto\Response\Traits\TimestampableTrait;

class ShortOrderDto implements ResponseDtoInterface
{
    use TimestampableTrait;

    /**
     * @param int $id
     * @param int $customerId
     * @param int $paymentMethodId
     * @param int $currencyId
     * @param string $localeCode
     * @param string $status
     * @param DateTimeInterface $releaseIn
     * @param int|null $amountBooked
     * @param int|null $amountPaid
     * @param int|null $amountCancelled
     * @param int|null $amountDiscount
     * @param string|null $paymentMethodName
     * @param string|null $agentName
     * @param string|null $agentEmail
     * @param string|null $customerEmail
     * @param int $agentId
     * @param string $currencyIso
     * @param bool $isTicketsAutoCancelled
     */
    public function __construct(
        private int $id,
        private int $customerId,
        private int $paymentMethodId,
        private int $currencyId,
        private string $localeCode,
        private string $status,
        private DateTimeInterface $releaseIn,
        private ?int $amountBooked,
        private ?int $amountPaid,
        private ?int $amountCancelled,
        private ?int $amountDiscount,
        private ?string $paymentMethodName,
        private ?string $agentName,
        private ?string $agentEmail,
        private ?string $customerEmail,
        private int $agentId,
        private string $currencyIso,
        private bool $isTicketsAutoCancelled,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCustomerId(): int
    {
        return $this->customerId;
    }

    public function getPaymentMethodId(): int
    {
        return $this->paymentMethodId;
    }

    public function getCurrencyId(): int
    {
        return $this->currencyId;
    }

    public function getLocaleCode(): string
    {
        return $this->localeCode;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getReleaseIn(): DateTimeInterface
    {
        return $this->releaseIn;
    }

    public function getAmountBooked(): ?int
    {
        return $this->amountBooked;
    }

    public function getAmountPaid(): ?int
    {
        return $this->amountPaid;
    }

    public function getAmountCancelled(): ?int
    {
        return $this->amountCancelled;
    }

    public function getAmountDiscount(): ?int
    {
        return $this->amountDiscount;
    }

    public function getPaymentMethodName(): ?string
    {
        return $this->paymentMethodName;
    }

    public function getAgentName(): ?string
    {
        return $this->agentName;
    }

    public function getAgentEmail(): ?string
    {
        return $this->agentEmail;
    }

    public function getCustomerEmail(): ?string
    {
        return $this->customerEmail;
    }

    public function getAgentId(): int
    {
        return $this->agentId;
    }

    public function getCurrencyIso(): string
    {
        return $this->currencyIso;
    }

    public function getIsTicketsAutoCancelled(): bool
    {
        return $this->isTicketsAutoCancelled;
    }
}
