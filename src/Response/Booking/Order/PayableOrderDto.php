<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking\Order;

use Sun\TransportBookingDto\Response\Booking\PaymentMethodDto;
use Sun\TransportBookingDto\Response\ResponseDtoInterface;

class PayableOrderDto implements ResponseDtoInterface
{
    /**
     * @param FullOrderDto $order
     * @param JourneyTicketDto[] $tickets
     * @param BaggageDto[] $baggage
     * @param TicketServiceDto[] $ticketServices
     * @param PaymentDto[] $payments
     * @param PaymentMethodDto[] $paymentMethods
     */
    public function __construct(
        private FullOrderDto $order,
        private array $tickets,
        private array $baggage,
        private array $ticketServices,
        private array $payments,
        private array $paymentMethods,
    ) {
    }

    public function getOrder(): FullOrderDto
    {
        return $this->order;
    }

    public function getTickets(): array
    {
        return $this->tickets;
    }

    public function getBaggage(): array
    {
        return $this->baggage;
    }

    public function getTicketServices(): array
    {
        return $this->ticketServices;
    }

    public function getPayments(): array
    {
        return $this->payments;
    }

    public function getPaymentMethods(): array
    {
        return $this->paymentMethods;
    }
}
