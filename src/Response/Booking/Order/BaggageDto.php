<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking\Order;

use DateTimeInterface;
use Sun\TransportBookingDto\Response\ResponseDtoInterface;

class BaggageDto implements ResponseDtoInterface
{
    public function __construct(
        private int $id,
        private int $baggageTypeId,
        private int $ticketId,
        private int $amount,
        private string $status,
        private DateTimeInterface $createdAt,
        private DateTimeInterface $updatedAt,
        private int $orderId,
        private int $journeyDateId,
        private int $currencyId,
        private int $customerId,
        private string $currencyIso,
        private string $customerEmail,
        private string $customerSurname,
        private string $customerName,
        private ?string $customerPatronymic,
        private int $agentId,
        private string $agentName,
        private string $agentEmail,
        private ?string $baggageTypeName,
        private ?string $baggageTypeDescription,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getBaggageTypeId(): int
    {
        return $this->baggageTypeId;
    }

    public function getTicketId(): int
    {
        return $this->ticketId;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function getOrderId(): int
    {
        return $this->orderId;
    }

    public function getJourneyDateId(): int
    {
        return $this->journeyDateId;
    }

    public function getCurrencyId(): int
    {
        return $this->currencyId;
    }

    public function getCustomerId(): int
    {
        return $this->customerId;
    }

    public function getCurrencyIso(): string
    {
        return $this->currencyIso;
    }

    public function getCustomerEmail(): string
    {
        return $this->customerEmail;
    }

    public function getCustomerSurname(): string
    {
        return $this->customerSurname;
    }

    public function getCustomerName(): string
    {
        return $this->customerName;
    }

    public function getCustomerPatronymic(): ?string
    {
        return $this->customerPatronymic;
    }

    public function getAgentId(): int
    {
        return $this->agentId;
    }

    public function getAgentName(): string
    {
        return $this->agentName;
    }

    public function getAgentEmail(): string
    {
        return $this->agentEmail;
    }

    public function getBaggageTypeName(): ?string
    {
        return $this->baggageTypeName;
    }

    public function getBaggageTypeDescription(): ?string
    {
        return $this->baggageTypeDescription;
    }
}
