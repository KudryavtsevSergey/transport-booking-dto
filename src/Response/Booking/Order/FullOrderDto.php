<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking\Order;

use DateTimeInterface;
use Sun\TransportBookingDto\Response\Booking\CustomerResponseDto;
use Sun\TransportBookingDto\Response\Booking\OrderJourneyDateTransferDto;
use Sun\TransportBookingDto\Response\NotificationProviderDto;
use Sun\TransportBookingDto\Response\PassengerDto;

class FullOrderDto extends ShortOrderDto
{
    /**
     * {@inheritdoc}
     * @param string|null $note
     * @param CustomerResponseDto $customer
     * @param PassengerDto[] $passengers
     * @param OrderPaymentMethodDto $paymentMethod
     * @param OrderJourneyDateTransferDto[] $orderJourneyDateTransfers
     * @param NotificationProviderDto[] $notificationProviders
     */
    public function __construct(
        int $id,
        int $customerId,
        int $paymentMethodId,
        int $currencyId,
        string $localeCode,
        string $status,
        DateTimeInterface $releaseIn,
        ?int $amountBooked,
        ?int $amountPaid,
        ?int $amountCancelled,
        ?int $amountDiscount,
        ?string $paymentMethodName,
        ?string $agentName,
        ?string $agentEmail,
        ?string $customerEmail,
        int $agentId,
        string $currencyIso,
        bool $isTicketsAutoCancelled,
        private ?string $note,
        private CustomerResponseDto $customer,
        private array $passengers,
        private OrderPaymentMethodDto $paymentMethod,
        private array $orderJourneyDateTransfers,
        private array $notificationProviders
    ) {
        parent::__construct(
            $id,
            $customerId,
            $paymentMethodId,
            $currencyId,
            $localeCode,
            $status,
            $releaseIn,
            $amountBooked,
            $amountPaid,
            $amountCancelled,
            $amountDiscount,
            $paymentMethodName,
            $agentName,
            $agentEmail,
            $customerEmail,
            $agentId,
            $currencyIso,
            $isTicketsAutoCancelled,
        );
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function getCustomer(): CustomerResponseDto
    {
        return $this->customer;
    }

    public function getPassengers(): array
    {
        return $this->passengers;
    }

    public function getPaymentMethod(): OrderPaymentMethodDto
    {
        return $this->paymentMethod;
    }

    public function getOrderJourneyDateTransfers(): array
    {
        return $this->orderJourneyDateTransfers;
    }

    public function getNotificationProviders(): array
    {
        return $this->notificationProviders;
    }
}
