<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking\Order;

class OrderPaymentTypeDto
{
    public function __construct(
        private int $id,
        private ?string $name,
        private string $code,
        private string $timeForPayment,
        private ?string $instruction,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getTimeForPayment(): string
    {
        return $this->timeForPayment;
    }

    public function getInstruction(): ?string
    {
        return $this->instruction;
    }
}
