<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking\Order;

use DateTimeInterface;
use Sun\TransportBookingDto\Response\JourneyDateScheduleResponseDto;
use Sun\TransportBookingDto\Response\ResponseDtoInterface;

class JourneyTicketDto implements ResponseDtoInterface
{
    public function __construct(
        private int $id,
        private int $passengerId,
        private int $ticketTypeId,
        private ?int $seatId,
        private int $amount,
        private string $status,
        private int $orderId,
        private int $fromStopId,
        private int $toStopId,
        private int $journeyDateId,
        private ?int $transportPartId,
        private int $seatTypeId,
        private DateTimeInterface $createdAt,
        private DateTimeInterface $updatedAt,
        private ?string $boardingStatus,
        private ?string $removalReason,
        private ?string $ticketTypeName,
        private ?string $transportPartName,
        private ?string $seatName,
        private string $ticketTypeCode,
        private bool $isWithoutSeat,
        private ?string $seatTypeName,
        private int $currencyId,
        private int $customerId,
        private string $currencyIso,
        private string $customerEmail,
        private string $customerSurname,
        private string $customerName,
        private ?string $customerPatronymic,
        private int $agentId,
        private string $agentName,
        private string $agentEmail,
        private string $passengerName,
        private string $passengerSurname,
        private ?string $passengerPatronymic,
        private string $passengerGender,
        private string $documentNumber,
        private int $documentTypeId,
        private string $documentTypeCode,
        private int $countryId,
        private ?string $countryName,
        private ?string $documentTypeName,
        private ?TicketPromoCodeDto $ticketPromoCode,
        private JourneyDateScheduleResponseDto $journeyDate,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPassengerId(): int
    {
        return $this->passengerId;
    }

    public function getTicketTypeId(): int
    {
        return $this->ticketTypeId;
    }

    public function getSeatId(): ?int
    {
        return $this->seatId;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getOrderId(): int
    {
        return $this->orderId;
    }

    public function getFromStopId(): int
    {
        return $this->fromStopId;
    }

    public function getToStopId(): int
    {
        return $this->toStopId;
    }

    public function getJourneyDateId(): int
    {
        return $this->journeyDateId;
    }

    public function getTransportPartId(): ?int
    {
        return $this->transportPartId;
    }

    public function getSeatTypeId(): int
    {
        return $this->seatTypeId;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function getBoardingStatus(): ?string
    {
        return $this->boardingStatus;
    }

    public function getRemovalReason(): ?string
    {
        return $this->removalReason;
    }

    public function getTicketTypeName(): ?string
    {
        return $this->ticketTypeName;
    }

    public function getTransportPartName(): ?string
    {
        return $this->transportPartName;
    }

    public function getSeatName(): ?string
    {
        return $this->seatName;
    }

    public function getTicketTypeCode(): string
    {
        return $this->ticketTypeCode;
    }

    public function getIsWithoutSeat(): bool
    {
        return $this->isWithoutSeat;
    }

    public function getSeatTypeName(): ?string
    {
        return $this->seatTypeName;
    }

    public function getCurrencyId(): int
    {
        return $this->currencyId;
    }

    public function getCustomerId(): int
    {
        return $this->customerId;
    }

    public function getCurrencyIso(): string
    {
        return $this->currencyIso;
    }

    public function getCustomerEmail(): string
    {
        return $this->customerEmail;
    }

    public function getCustomerSurname(): string
    {
        return $this->customerSurname;
    }

    public function getCustomerName(): string
    {
        return $this->customerName;
    }

    public function getCustomerPatronymic(): ?string
    {
        return $this->customerPatronymic;
    }

    public function getAgentId(): int
    {
        return $this->agentId;
    }

    public function getAgentName(): string
    {
        return $this->agentName;
    }

    public function getAgentEmail(): string
    {
        return $this->agentEmail;
    }

    public function getPassengerName(): string
    {
        return $this->passengerName;
    }

    public function getPassengerSurname(): string
    {
        return $this->passengerSurname;
    }

    public function getPassengerPatronymic(): ?string
    {
        return $this->passengerPatronymic;
    }

    public function getPassengerGender(): string
    {
        return $this->passengerGender;
    }

    public function getDocumentNumber(): string
    {
        return $this->documentNumber;
    }

    public function getDocumentTypeId(): int
    {
        return $this->documentTypeId;
    }

    public function getDocumentTypeCode(): string
    {
        return $this->documentTypeCode;
    }

    public function getCountryId(): int
    {
        return $this->countryId;
    }

    public function getCountryName(): ?string
    {
        return $this->countryName;
    }

    public function getDocumentTypeName(): ?string
    {
        return $this->documentTypeName;
    }

    public function getTicketPromoCode(): ?TicketPromoCodeDto
    {
        return $this->ticketPromoCode;
    }

    public function getJourneyDate(): JourneyDateScheduleResponseDto
    {
        return $this->journeyDate;
    }
}
