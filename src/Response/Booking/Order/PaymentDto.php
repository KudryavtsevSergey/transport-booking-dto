<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking\Order;

use DateTimeInterface;

class PaymentDto
{
    public function __construct(
        private int $id,
        private int $amount,
        private int $paymentTypeId,
        private int $currencyId,
        private int $orderId,
        private string $status,
        private DateTimeInterface $expiresAt,
        private ?DateTimeInterface $changedAt,
        private DateTimeInterface $createdAt,
        private string $currencyIso,
        private ?string $paymentTypeName,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getPaymentTypeId(): int
    {
        return $this->paymentTypeId;
    }

    public function getCurrencyId(): int
    {
        return $this->currencyId;
    }

    public function getOrderId(): int
    {
        return $this->orderId;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getExpiresAt(): DateTimeInterface
    {
        return $this->expiresAt;
    }

    public function getChangedAt(): ?DateTimeInterface
    {
        return $this->changedAt;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getCurrencyIso(): string
    {
        return $this->currencyIso;
    }

    public function getPaymentTypeName(): ?string
    {
        return $this->paymentTypeName;
    }
}
