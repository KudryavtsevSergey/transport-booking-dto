<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking\Order;

use Sun\TransportBookingDto\Response\Booking\PaymentMethodDto;

class OrderPaymentMethodDto extends PaymentMethodDto
{
    /**
     * @param int $id
     * @param string|null $name
     * @param bool $isTicketsAutoCancelled
     * @param OrderPaymentTypeDto[] $paymentTypes
     */
    public function __construct(
        int $id,
        ?string $name,
        bool $isTicketsAutoCancelled,
        private array $paymentTypes,
    ) {
        parent::__construct($id, $name, $isTicketsAutoCancelled);
    }

    public function getPaymentTypes(): array
    {
        return $this->paymentTypes;
    }
}
