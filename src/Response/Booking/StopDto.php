<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking;

use Sun\TransportBookingDto\Response\ResponseDtoInterface;

class StopDto implements ResponseDtoInterface
{
    public function __construct(
        private int $id,
        private float $latitude,
        private float $longitude,
        private ?string $name,
        private int $cityId,
        private ?string $cityName,
        private bool $isNeedTransfer,
        private int $stopTypeId,
        private ?string $stopTypeName,
        private string $timezone,
        private ?string $fullName,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getCityId(): int
    {
        return $this->cityId;
    }

    public function getCityName(): ?string
    {
        return $this->cityName;
    }

    public function getIsNeedTransfer(): bool
    {
        return $this->isNeedTransfer;
    }

    public function getStopTypeId(): int
    {
        return $this->stopTypeId;
    }

    public function getStopTypeName(): ?string
    {
        return $this->stopTypeName;
    }

    public function getTimezone(): string
    {
        return $this->timezone;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }
}
