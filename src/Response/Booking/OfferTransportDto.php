<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking;

class OfferTransportDto extends AbstractTransportDto
{
    /**
     * @param int $id
     * @param bool $isWithSeating
     * @param string|null $name
     * @param string $schemaType
     * @param string|null $image
     * @param SeatDto[] $seats
     * @param SchemaTransportRowDto[] $schemaTransportRows
     * @param TransportPartDto[] $transportParts
     */
    public function __construct(
        int $id,
        ?string $name,
        private bool $isWithSeating,
        private string $schemaType,
        ?string $image = null,
        array $seats = [],
        array $schemaTransportRows = [],
        private array $transportParts = [],
    ) {
        parent::__construct(
            $id,
            $name,
            $image,
            $seats,
            $schemaTransportRows,
        );
    }

    public function getIsWithSeating(): bool
    {
        return $this->isWithSeating;
    }

    public function getSchemaType(): string
    {
        return $this->schemaType;
    }

    public function getTransportParts(): array
    {
        return $this->transportParts;
    }
}
