<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking;

class SchemaTransportSeatDto
{
    public function __construct(
        private int $id,
        private string $type,
        private ?int $seatId,
        private ?string $name,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getSeatId(): ?int
    {
        return $this->seatId;
    }

    public function getName(): ?string
    {
        return $this->name;
    }
}
