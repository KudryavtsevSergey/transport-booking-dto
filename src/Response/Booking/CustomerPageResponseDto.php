<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking;

use Sun\TransportBookingDto\Response\PageResponseDto;

class CustomerPageResponseDto extends PageResponseDto
{
    /**
     * @param int $currentPage
     * @param int $perPage
     * @param int $total
     * @param CustomerResponseDto[] $data
     */
    public function __construct(int $currentPage, int $perPage, int $total, array $data)
    {
        parent::__construct($currentPage, $perPage, $total, $data);
    }
}
