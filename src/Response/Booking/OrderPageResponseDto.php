<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking;

use Sun\TransportBookingDto\Response\Booking\Order\ShortOrderDto;
use Sun\TransportBookingDto\Response\PageResponseDto;

class OrderPageResponseDto extends PageResponseDto
{
    /**
     * @param int $currentPage
     * @param int $perPage
     * @param int $total
     * @param ShortOrderDto[] $data
     */
    public function __construct(int $currentPage, int $perPage, int $total, array $data)
    {
        parent::__construct($currentPage, $perPage, $total, $data);
    }
}
