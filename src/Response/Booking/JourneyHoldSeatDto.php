<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking;

use Sun\TransportBookingDto\Response\ResponseDtoInterface;

class JourneyHoldSeatDto implements ResponseDtoInterface
{
    public function __construct(
        private int $id,
        private ?int $seatId,
        private int $fromStopId,
        private int $ticketTypeId,
        private int $toStopId,
        private ?int $transportPartId,
        private int $journeyDateId,
        private int $seatTypeId,
        private ?string $transportPartName,
        private ?string $seatName,
        private ?string $ticketTypeName,
        private int $amount,
        private ?HoldSeatPromoCodeDto $holdSeatPromoCode = null
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getSeatId(): ?int
    {
        return $this->seatId;
    }

    public function getFromStopId(): int
    {
        return $this->fromStopId;
    }

    public function getTicketTypeId(): int
    {
        return $this->ticketTypeId;
    }

    public function getToStopId(): int
    {
        return $this->toStopId;
    }

    public function getTransportPartId(): ?int
    {
        return $this->transportPartId;
    }

    public function getJourneyDateId(): int
    {
        return $this->journeyDateId;
    }

    public function getSeatTypeId(): int
    {
        return $this->seatTypeId;
    }

    public function getTransportPartName(): ?string
    {
        return $this->transportPartName;
    }

    public function getSeatName(): ?string
    {
        return $this->seatName;
    }

    public function getTicketTypeName(): ?string
    {
        return $this->ticketTypeName;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getHoldSeatPromoCode(): ?HoldSeatPromoCodeDto
    {
        return $this->holdSeatPromoCode;
    }
}
