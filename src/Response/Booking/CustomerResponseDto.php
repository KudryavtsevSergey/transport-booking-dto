<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking;

use Sun\TransportBookingDto\Response\ResponseDtoInterface;
use Sun\TransportBookingDto\Response\Traits\TimestampableTrait;

class CustomerResponseDto implements ResponseDtoInterface
{
    use TimestampableTrait;

    /**
     * @param int $id
     * @param string|null $fullName
     * @param string $name
     * @param string $surname
     * @param string|null $patronymic
     * @param string $email
     * @param int $agentId
     * @param PhoneDto[] $phones
     */
    public function __construct(
        private int $id,
        private ?string $fullName,
        private string $name,
        private string $surname,
        private ?string $patronymic,
        private string $email,
        private int $agentId,
        private array $phones,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getAgentId(): int
    {
        return $this->agentId;
    }

    public function getPhones(): array
    {
        return $this->phones;
    }
}
