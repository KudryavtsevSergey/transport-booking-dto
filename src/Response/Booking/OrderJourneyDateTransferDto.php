<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking;

class OrderJourneyDateTransferDto
{
    public function __construct(
        private int $journeyDateId,
        private int $stopId,
        private string $transferTime,
    ) {
    }

    public function getJourneyDateId(): int
    {
        return $this->journeyDateId;
    }

    public function getStopId(): int
    {
        return $this->stopId;
    }

    public function getTransferTime(): string
    {
        return $this->transferTime;
    }
}
