<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking;

use Sun\TransportBookingDto\Response\ResponseDtoInterface;

class CurrencyDto implements ResponseDtoInterface
{
    public function __construct(
        private int $id,
        private string $iso,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getIso(): string
    {
        return $this->iso;
    }
}
