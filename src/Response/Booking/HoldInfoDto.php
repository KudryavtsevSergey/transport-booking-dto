<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking;

use DateTimeInterface;
use Sun\TransportBookingDto\Response\ResponseDtoInterface;

class HoldInfoDto implements ResponseDtoInterface
{
    /**
     * @param int $id
     * @param DateTimeInterface $releaseIn
     * @param int $agentId
     * @param int $currencyId
     * @param DateTimeInterface $createdAt
     * @param string $currencyIso
     * @param string $agentName
     * @param string $agentEmail
     * @param JourneyHoldDto[] $journeyHolds
     * @param PaymentMethodDto[] $paymentMethods
     */
    public function __construct(
        private int $id,
        private DateTimeInterface $releaseIn,
        private int $agentId,
        private int $currencyId,
        private DateTimeInterface $createdAt,
        private string $currencyIso,
        private string $agentName,
        private string $agentEmail,
        private array $journeyHolds,
        private array $paymentMethods,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getReleaseIn(): DateTimeInterface
    {
        return $this->releaseIn;
    }

    public function getAgentId(): int
    {
        return $this->agentId;
    }

    public function getCurrencyId(): int
    {
        return $this->currencyId;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getCurrencyIso(): string
    {
        return $this->currencyIso;
    }

    public function getAgentName(): string
    {
        return $this->agentName;
    }

    public function getAgentEmail(): string
    {
        return $this->agentEmail;
    }

    public function getJourneyHolds(): array
    {
        return $this->journeyHolds;
    }

    public function getPaymentMethods(): array
    {
        return $this->paymentMethods;
    }
}
