<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking;

use Sun\TransportBookingDto\Response\DriverPhoneDto;
use Sun\TransportBookingDto\Response\SocialProviderDto;

class PhoneDto extends DriverPhoneDto
{
    /**
     * @param int $id
     * @param string $number
     * @param int $countryId
     * @param string $phoneCode
     * @param string $alpha2
     * @param string $fullNumber
     * @param bool $useForNotifications
     * @param SocialProviderDto[] $socialProviders
     */
    public function __construct(
        int $id,
        string $number,
        int $countryId,
        string $phoneCode,
        string $alpha2,
        string $fullNumber,
        private bool $useForNotifications,
        private array $socialProviders
    ) {
        parent::__construct(
            $id,
            $number,
            $countryId,
            $phoneCode,
            $alpha2,
            $fullNumber
        );
    }

    public function getUseForNotifications(): bool
    {
        return $this->useForNotifications;
    }

    public function getSocialProviders(): array
    {
        return $this->socialProviders;
    }
}
