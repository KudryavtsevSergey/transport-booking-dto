<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking;

use Sun\TransportBookingDto\Response\ResponseDtoInterface;

class PaymentMethodDto implements ResponseDtoInterface
{
    public function __construct(
        private int $id,
        private ?string $name,
        private bool $isTicketsAutoCancelled,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getIsTicketsAutoCancelled(): bool
    {
        return $this->isTicketsAutoCancelled;
    }
}
