<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking;

use Sun\TransportBookingDto\Response\ResponseDtoInterface;

class OfferDto implements ResponseDtoInterface
{
    public function __construct(
        private DirectOfferDto $forward,
        private ?DirectOfferDto $back,
    ) {
    }

    public function getForward(): DirectOfferDto
    {
        return $this->forward;
    }

    public function getBack(): ?DirectOfferDto
    {
        return $this->back;
    }
}
