<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking;

class TransportPartDto extends AbstractTransportDto
{
    /**
     * @param int $id
     * @param string|null $name
     * @param int $order
     * @param string|null $image
     * @param SeatDto[] $seats
     * @param SchemaTransportRowDto[] $schemaTransportRows
     */
    public function __construct(
        int $id,
        ?string $name,
        private int $order,
        ?string $image = null,
        array $seats = [],
        array $schemaTransportRows = [],
    ) {
        parent::__construct(
            $id,
            $name,
            $image,
            $seats,
            $schemaTransportRows,
        );
    }

    public function getOrder(): int
    {
        return $this->order;
    }
}
