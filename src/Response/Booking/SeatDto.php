<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking;

class SeatDto
{
    public function __construct(
        private int $id,
        private ?int $width,
        private ?int $height,
        private ?int $top,
        private ?int $left,
        private string $name,
        private int $seatTypeId,
        private string $seatTypeCode,
        private bool $held,
        private bool $booked,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function getTop(): ?int
    {
        return $this->top;
    }

    public function getLeft(): ?int
    {
        return $this->left;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSeatTypeId(): int
    {
        return $this->seatTypeId;
    }

    public function getSeatTypeCode(): string
    {
        return $this->seatTypeCode;
    }

    public function getHeld(): bool
    {
        return $this->held;
    }

    public function getBooked(): bool
    {
        return $this->booked;
    }
}
