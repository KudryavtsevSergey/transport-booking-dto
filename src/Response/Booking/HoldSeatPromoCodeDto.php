<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking;

use DateTimeInterface;

class HoldSeatPromoCodeDto
{
    public function __construct(
        private int $promoCodeId,
        private string $promoCodeName,
        private int $promoJourneyDateId,
        private int $discount,
        private DateTimeInterface $createdAt,
    ) {
    }

    public function getPromoCodeId(): int
    {
        return $this->promoCodeId;
    }

    public function getPromoCodeName(): string
    {
        return $this->promoCodeName;
    }

    public function getPromoJourneyDateId(): int
    {
        return $this->promoJourneyDateId;
    }

    public function getDiscount(): int
    {
        return $this->discount;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }
}
