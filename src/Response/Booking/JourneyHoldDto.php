<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking;

use Sun\TransportBookingDto\Response\BaggageTypePriceDto;
use Sun\TransportBookingDto\Response\DriverDto;
use Sun\TransportBookingDto\Response\JourneyBaggageTypeDto;
use Sun\TransportBookingDto\Response\JourneyDateSeatTypeDto;
use Sun\TransportBookingDto\Response\JourneyTicketServiceDto;
use Sun\TransportBookingDto\Response\PaidServicePriceDto;
use Sun\TransportBookingDto\Response\PromoJourneyDateDto;
use Sun\TransportBookingDto\Response\RoutePriceDto;

class JourneyHoldDto extends DirectOfferDto
{
    /**
     * @param int $journeyId
     * @param string $date
     * @param int $journeyDateId
     * @param bool $isSaleProhibited
     * @param bool $isBoardingFinished
     * @param int $carrierId
     * @param string|null $carrierName
     * @param int $routeId
     * @param string|null $routeName
     * @param int $ticketsCount
     * @param int $holdSeatsCount
     * @param int $transportId
     * @param string|null $transportName
     * @param int $transportPartsSeatsCount
     * @param JourneyDateStopDto $start
     * @param JourneyDateStopDto $from
     * @param JourneyDateStopDto $to
     * @param JourneyDateStopDto $finish
     * @param JourneyDateStopDto[] $journeyStops
     * @param RoutePriceDto[] $prices
     * @param DriverDto[] $drivers
     * @param PromoJourneyDateDto[] $promoJourneyDates
     * @param JourneyBaggageTypeDto[] $journeyBaggageTypes
     * @param JourneyTicketServiceDto[] $journeyTicketServices
     * @param BaggageTypePriceDto[] $baggagePrices
     * @param PaidServicePriceDto[] $paidServicePrices
     * @param JourneyDateSeatTypeDto[] $journeyDateSeatTypes
     * @param OfferServiceDto[] $services
     * @param OfferTransportDto $transport
     * @param JourneyHoldSeatDto[] $seats
     */
    public function __construct(
        int $journeyId,
        string $date,
        int $journeyDateId,
        bool $isSaleProhibited,
        bool $isBoardingFinished,
        int $carrierId,
        ?string $carrierName,
        int $routeId,
        ?string $routeName,
        int $ticketsCount,
        int $holdSeatsCount,
        int $transportId,
        ?string $transportName,
        int $transportPartsSeatsCount,
        JourneyDateStopDto $start,
        JourneyDateStopDto $from,
        JourneyDateStopDto $to,
        JourneyDateStopDto $finish,
        array $journeyStops,
        array $prices,
        array $drivers,
        array $promoJourneyDates,
        array $journeyBaggageTypes,
        array $journeyTicketServices,
        array $baggagePrices,
        array $paidServicePrices,
        array $journeyDateSeatTypes,
        array $services,
        OfferTransportDto $transport,
        private array $seats,
    ) {
        parent::__construct(
            $journeyId,
            $date,
            $journeyDateId,
            $isSaleProhibited,
            $isBoardingFinished,
            $carrierId,
            $carrierName,
            $routeId,
            $routeName,
            $ticketsCount,
            $holdSeatsCount,
            $transportId,
            $transportName,
            $transportPartsSeatsCount,
            $start,
            $from,
            $to,
            $finish,
            $journeyStops,
            $prices,
            $drivers,
            $promoJourneyDates,
            $journeyBaggageTypes,
            $journeyTicketServices,
            $baggagePrices,
            $paidServicePrices,
            $journeyDateSeatTypes,
            $services,
            $transport
        );
    }

    public function getSeats(): array
    {
        return $this->seats;
    }
}
