<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Booking;

abstract class AbstractTransportDto
{
    /**
     * @param int $id
     * @param string|null $name
     * @param string|null $image
     * @param SeatDto[] $seats
     * @param SchemaTransportRowDto[] $schemaTransportRows
     */
    public function __construct(
        private int $id,
        private ?string $name,
        private ?string $image = null,
        private array $seats = [],
        private array $schemaTransportRows = [],
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function getSeats(): array
    {
        return $this->seats;
    }

    public function getSchemaTransportRows(): array
    {
        return $this->schemaTransportRows;
    }
}
