<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

use DateTimeInterface;

class PromoDataDto
{
    /**
     * @param int $id
     * @param string|null $name
     * @param int|null $numberOfUses
     * @param DateTimeInterface|null $startDateBooking
     * @param DateTimeInterface|null $endDateBooking
     * @param DiscountDto[] $discounts
     * @param PromoTicketTypeDto[] $promoTicketTypes
     */
    public function __construct(
        private int $id,
        private ?string $name,
        private ?int $numberOfUses,
        private ?DateTimeInterface $startDateBooking,
        private ?DateTimeInterface $endDateBooking,
        private array $discounts,
        private array $promoTicketTypes,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getNumberOfUses(): ?int
    {
        return $this->numberOfUses;
    }

    public function getEndDateBooking(): DateTimeInterface
    {
        return $this->endDateBooking;
    }

    public function getStartDateBooking(): DateTimeInterface
    {
        return $this->startDateBooking;
    }

    public function getDiscounts(): array
    {
        return $this->discounts;
    }

    public function getPromoTicketTypes(): array
    {
        return $this->promoTicketTypes;
    }
}
