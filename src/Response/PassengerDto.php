<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

class PassengerDto implements ResponseDtoInterface
{
    public function __construct(
        private int $id,
        private ?string $fullName,
        private string $surname,
        private string $name,
        private ?string $patronymic,
        private int $countryId,
        private ?string $countryName,
        private int $customerId,
        private string $documentNumber,
        private string $documentTypeCode,
        private int $documentTypeId,
        private ?string $documentTypeName,
        private string $gender,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    public function getCountryId(): int
    {
        return $this->countryId;
    }

    public function getCountryName(): ?string
    {
        return $this->countryName;
    }

    public function getCustomerId(): int
    {
        return $this->customerId;
    }

    public function getDocumentNumber(): string
    {
        return $this->documentNumber;
    }

    public function getDocumentTypeCode(): string
    {
        return $this->documentTypeCode;
    }

    public function getDocumentTypeId(): int
    {
        return $this->documentTypeId;
    }

    public function getDocumentTypeName(): ?string
    {
        return $this->documentTypeName;
    }

    public function getGender(): string
    {
        return $this->gender;
    }
}
