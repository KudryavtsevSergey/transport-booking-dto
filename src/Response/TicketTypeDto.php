<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

class TicketTypeDto implements ResponseDtoInterface
{
    public function __construct(
        private int $id,
        private ?string $name,
        private string $code,
        private bool $isWithoutSeat,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getIsWithoutSeat(): bool
    {
        return $this->isWithoutSeat;
    }
}
