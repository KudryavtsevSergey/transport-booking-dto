<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

class DriverDto
{
    /**
     * @param int $id
     * @param string|null $fullName
     * @param string|null $name
     * @param string|null $surname
     * @param string|null $patronymic
     * @param string $documentNumber
     * @param string $gender
     * @param int $documentTypeId
     * @param int $carrierId
     * @param string $documentTypeCode
     * @param int $countryId
     * @param string|null $countryName
     * @param string|null $documentTypeName
     * @param string|null $carrierName
     * @param DriverPhoneDto[] $driverPhones
     */
    public function __construct(
        private int $id,
        private ?string $fullName,
        private ?string $name,
        private ?string $surname,
        private ?string $patronymic,
        private string $documentNumber,
        private string $gender,
        private int $documentTypeId,
        private int $carrierId,
        private string $documentTypeCode,
        private int $countryId,
        private ?string $countryName,
        private ?string $documentTypeName,
        private ?string $carrierName,
        private array $driverPhones,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    public function getDocumentNumber(): string
    {
        return $this->documentNumber;
    }

    public function getGender(): string
    {
        return $this->gender;
    }

    public function getDocumentTypeId(): int
    {
        return $this->documentTypeId;
    }

    public function getCarrierId(): int
    {
        return $this->carrierId;
    }

    public function getDocumentTypeCode(): string
    {
        return $this->documentTypeCode;
    }

    public function getCountryId(): int
    {
        return $this->countryId;
    }

    public function getCountryName(): ?string
    {
        return $this->countryName;
    }

    public function getDocumentTypeName(): ?string
    {
        return $this->documentTypeName;
    }

    public function getCarrierName(): ?string
    {
        return $this->carrierName;
    }

    public function getDriverPhones(): array
    {
        return $this->driverPhones;
    }
}
