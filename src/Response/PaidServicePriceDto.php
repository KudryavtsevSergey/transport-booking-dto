<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

class PaidServicePriceDto
{
    /**
     * @param int $paidServicePriceId
     * @param int $paidServiceId
     * @param PaidServicePriceCurrencyDto[] $prices
     */
    public function __construct(
        private int $paidServicePriceId,
        private int $paidServiceId,
        private array $prices,
    ) {
    }

    public function getPaidServicePriceId(): int
    {
        return $this->paidServicePriceId;
    }

    public function getPaidServiceId(): int
    {
        return $this->paidServiceId;
    }

    public function getPrices(): array
    {
        return $this->prices;
    }
}
