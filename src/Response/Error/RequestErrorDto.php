<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response\Error;

use Sun\TransportBookingDto\Response\ResponseDtoInterface;
use Sun\TransportBookingDto\Enum\ErrorCodeEnum;

class RequestErrorDto implements ResponseDtoInterface
{
    public function __construct(
        private string $code,
        private string $message,
        private array $errors = [],
    ) {
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function isUnauthenticated(): bool
    {
        return $this->code === ErrorCodeEnum::UNAUTHENTICATED;
    }

    public function isValidation(): bool
    {
        return $this->code === ErrorCodeEnum::VALIDATION_ERROR;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}
