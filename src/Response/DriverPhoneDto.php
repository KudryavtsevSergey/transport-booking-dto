<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

class DriverPhoneDto
{
    public function __construct(
        private int $id,
        private string $number,
        private int $countryId,
        private string $phoneCode,
        private string $alpha2,
        private string $fullNumber,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function getCountryId(): int
    {
        return $this->countryId;
    }

    public function getPhoneCode(): string
    {
        return $this->phoneCode;
    }

    public function getAlpha2(): string
    {
        return $this->alpha2;
    }

    public function getFullNumber(): string
    {
        return $this->fullNumber;
    }
}
