<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

class DocumentTypeDto implements ResponseDtoInterface
{
    public function __construct(
        private int $id,
        private int $countryId,
        private string $code,
        private ?string $name,
        private ?string $countryName,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCountryId(): int
    {
        return $this->countryId;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getCountryName(): ?string
    {
        return $this->countryName;
    }
}
