<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

use Sun\TransportBookingDto\Response\Booking\StopDto;

class RouteStopDto
{
    public function __construct(
        private StopDto $fromStop,
        private StopDto $toStop,
    ) {
    }

    public function getFromStop(): StopDto
    {
        return $this->fromStop;
    }

    public function getToStop(): StopDto
    {
        return $this->toStop;
    }
}
