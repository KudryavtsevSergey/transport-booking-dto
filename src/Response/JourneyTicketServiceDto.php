<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

class JourneyTicketServiceDto
{
    public function __construct(
        private int $journeyId,
        private int $paidServiceId,
        private int $paidServicePriceId,
        private ?string $paidServiceName,
    ) {
    }

    public function getJourneyId(): int
    {
        return $this->journeyId;
    }

    public function getPaidServiceId(): int
    {
        return $this->paidServiceId;
    }

    public function getPaidServicePriceId(): int
    {
        return $this->paidServicePriceId;
    }

    public function getPaidServiceName(): ?string
    {
        return $this->paidServiceName;
    }
}
