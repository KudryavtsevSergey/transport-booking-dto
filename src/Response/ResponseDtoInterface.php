<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

use Sun\TransportBookingDto\DtoInterface;

interface ResponseDtoInterface extends DtoInterface
{

}
