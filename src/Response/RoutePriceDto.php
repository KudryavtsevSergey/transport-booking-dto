<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

class RoutePriceDto
{
    /**
     * @param int $routePriceId
     * @param RoutePriceCurrencyDto[] $routePriceCurrencies
     */
    public function __construct(
        private int $routePriceId,
        private array $routePriceCurrencies,
    ) {
    }

    public function getRoutePriceId(): int
    {
        return $this->routePriceId;
    }

    public function getRoutePriceCurrencies(): array
    {
        return $this->routePriceCurrencies;
    }
}
