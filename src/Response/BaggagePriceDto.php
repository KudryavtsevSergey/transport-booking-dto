<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

class BaggagePriceDto
{
    public function __construct(
        private int $baggageTypePriceId,
        private int $amount,
        private int $currencyId,
    ) {
    }

    public function getBaggageTypePriceId(): int
    {
        return $this->baggageTypePriceId;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getCurrencyId(): int
    {
        return $this->currencyId;
    }
}
