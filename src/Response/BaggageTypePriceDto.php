<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

class BaggageTypePriceDto
{
    /**
     * @param int $baggageTypePriceId
     * @param int $baggageTypeId
     * @param BaggagePriceDto[] $prices
     */
    public function __construct(
        private int $baggageTypePriceId,
        private int $baggageTypeId,
        private array $prices,
    ) {
    }

    public function getBaggageTypePriceId(): int
    {
        return $this->baggageTypePriceId;
    }

    public function getBaggageTypeId(): int
    {
        return $this->baggageTypeId;
    }

    public function getPrices(): array
    {
        return $this->prices;
    }
}
