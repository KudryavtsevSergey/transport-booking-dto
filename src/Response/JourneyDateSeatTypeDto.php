<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

class JourneyDateSeatTypeDto
{
    public function __construct(
        private int $journeyDateId,
        private int $seatTypeId,
        private int $routePriceId,
        private ?string $seatTypeName,
        private string $seatTypeCode,
    ) {
    }

    public function getJourneyDateId(): int
    {
        return $this->journeyDateId;
    }

    public function getSeatTypeId(): int
    {
        return $this->seatTypeId;
    }

    public function getRoutePriceId(): int
    {
        return $this->routePriceId;
    }

    public function getSeatTypeName(): ?string
    {
        return $this->seatTypeName;
    }

    public function getSeatTypeCode(): string
    {
        return $this->seatTypeCode;
    }
}
