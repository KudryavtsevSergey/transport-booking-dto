<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

class PaymentLinkResponseDto implements ResponseDtoInterface
{
    public function __construct(
        private string $uri,
    ) {
    }

    public function getUri(): string
    {
        return $this->uri;
    }
}
