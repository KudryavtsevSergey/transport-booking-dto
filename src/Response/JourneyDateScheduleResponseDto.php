<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Response;

use Sun\TransportBookingDto\Response\Booking\JourneyDateStopDto;

class JourneyDateScheduleResponseDto implements ResponseDtoInterface
{
    /**
     * @param int $journeyId
     * @param string $date
     * @param int $journeyDateId
     * @param bool $isSaleProhibited
     * @param bool $isBoardingFinished
     * @param int $carrierId
     * @param string|null $carrierName
     * @param int $routeId
     * @param string|null $routeName
     * @param int $ticketsCount
     * @param int $holdSeatsCount
     * @param int $transportId
     * @param string|null $transportName
     * @param int $transportPartsSeatsCount
     * @param JourneyDateStopDto $start
     * @param JourneyDateStopDto $from
     * @param JourneyDateStopDto $to
     * @param JourneyDateStopDto $finish
     * @param JourneyDateStopDto[] $journeyStops
     * @param RoutePriceDto[] $prices
     * @param DriverDto[] $drivers
     * @param PromoJourneyDateDto[] $promoJourneyDates
     * @param JourneyBaggageTypeDto[] $journeyBaggageTypes
     * @param JourneyTicketServiceDto[] $journeyTicketServices
     * @param BaggageTypePriceDto[] $baggagePrices
     * @param PaidServicePriceDto[] $paidServicePrices
     * @param JourneyDateSeatTypeDto[] $journeyDateSeatTypes
     */
    public function __construct(
        private int $journeyId,
        private string $date,
        private int $journeyDateId,
        private bool $isSaleProhibited,
        private bool $isBoardingFinished,
        private int $carrierId,
        private ?string $carrierName,
        private int $routeId,
        private ?string $routeName,
        private int $ticketsCount,
        private int $holdSeatsCount,
        private int $transportId,
        private ?string $transportName,
        private int $transportPartsSeatsCount,
        private JourneyDateStopDto $start,
        private JourneyDateStopDto $from,
        private JourneyDateStopDto $to,
        private JourneyDateStopDto $finish,
        private array $journeyStops,
        private array $prices,
        private array $drivers,
        private array $promoJourneyDates,
        private array $journeyBaggageTypes,
        private array $journeyTicketServices,
        private array $baggagePrices,
        private array $paidServicePrices,
        private array $journeyDateSeatTypes,
    ) {
    }

    public function getJourneyId(): int
    {
        return $this->journeyId;
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function getJourneyDateId(): int
    {
        return $this->journeyDateId;
    }

    public function getIsSaleProhibited(): bool
    {
        return $this->isSaleProhibited;
    }

    public function getIsBoardingFinished(): bool
    {
        return $this->isBoardingFinished;
    }

    public function getCarrierId(): int
    {
        return $this->carrierId;
    }

    public function getCarrierName(): ?string
    {
        return $this->carrierName;
    }

    public function getRouteId(): int
    {
        return $this->routeId;
    }

    public function getRouteName(): ?string
    {
        return $this->routeName;
    }

    public function getTicketsCount(): int
    {
        return $this->ticketsCount;
    }

    public function getHoldSeatsCount(): int
    {
        return $this->holdSeatsCount;
    }

    public function getTransportId(): int
    {
        return $this->transportId;
    }

    public function getTransportName(): ?string
    {
        return $this->transportName;
    }

    public function getTransportPartsSeatsCount(): int
    {
        return $this->transportPartsSeatsCount;
    }

    public function getStart(): JourneyDateStopDto
    {
        return $this->start;
    }

    public function getFrom(): JourneyDateStopDto
    {
        return $this->from;
    }

    public function getTo(): JourneyDateStopDto
    {
        return $this->to;
    }

    public function getFinish(): JourneyDateStopDto
    {
        return $this->finish;
    }

    public function getJourneyStops(): array
    {
        return $this->journeyStops;
    }

    public function getPrices(): array
    {
        return $this->prices;
    }

    public function getDrivers(): array
    {
        return $this->drivers;
    }

    public function getPromoJourneyDates(): array
    {
        return $this->promoJourneyDates;
    }

    public function getJourneyBaggageTypes(): array
    {
        return $this->journeyBaggageTypes;
    }

    public function getJourneyTicketServices(): array
    {
        return $this->journeyTicketServices;
    }

    public function getBaggagePrices(): array
    {
        return $this->baggagePrices;
    }

    public function getPaidServicePrices(): array
    {
        return $this->paidServicePrices;
    }

    public function getJourneyDateSeatTypes(): array
    {
        return $this->journeyDateSeatTypes;
    }
}
