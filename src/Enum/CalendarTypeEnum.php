<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Enum;

class CalendarTypeEnum
{
    public const DAY = 'day';
    public const WEEK = 'week';
    public const MONTH = 'month';
}
