<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request;

use Sun\TransportBookingDto\Request\Filter\FilterInterface;
use Sun\TransportBookingDto\Request\Filter\SearchDto;

class PageRequestDto extends SearchDto
{
    public function __construct(private PaginationDto $pagination, ?FilterInterface $search)
    {
        parent::__construct($search);
    }

    public function getPagination(): PaginationDto
    {
        return $this->pagination;
    }
}
