<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request;

use Sun\TransportBookingDto\Request\Booking\PassengerDto;

class FullPassengerRequestDto extends PassengerDto implements BodyDtoInterface
{
    public function __construct(
        string $surname,
        string $name,
        ?string $patronymic,
        string $gender,
        string $documentNumber,
        int $documentTypeId,
        private int $customerId,
    ) {
        parent::__construct(
            null,
            $surname,
            $name,
            $patronymic,
            $gender,
            $documentNumber,
            $documentTypeId,
        );
    }

    public function getCustomerId(): int
    {
        return $this->customerId;
    }
}
