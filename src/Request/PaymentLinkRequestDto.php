<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request;

class PaymentLinkRequestDto implements QueryDtoInterface
{
    public function __construct(
        private readonly string $callback,
    ) {
    }

    public function getCallback(): string
    {
        return $this->callback;
    }
}
