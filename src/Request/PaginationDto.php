<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request;

class PaginationDto implements QueryDtoInterface
{
    private const DEFAULT_ITEMS_PER_PAGE = 20;
    private const DEFAULT_PAGE = 0;

    public function __construct(
        private int $itemsPerPage = self::DEFAULT_ITEMS_PER_PAGE,
        private int $page = self::DEFAULT_PAGE,
        private array $sortBy = [],
        private array $sortDesc = [],
    ) {
    }

    public function getItemsPerPage(): int
    {
        return $this->itemsPerPage;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getSortBy(): array
    {
        return $this->sortBy;
    }

    public function getSortDesc(): array
    {
        return $this->sortDesc;
    }
}
