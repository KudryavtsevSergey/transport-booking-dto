<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Filter;

use Sun\TransportBookingDto\Request\QueryDtoInterface;

class SearchDto implements QueryDtoInterface
{
    public function __construct(
        private ?FilterInterface $search,
    ) {
    }

    public function getSearch(): ?FilterInterface
    {
        return $this->search;
    }
}
