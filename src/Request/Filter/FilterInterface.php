<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Filter;

use Sun\TransportBookingDto\Request\RequestDtoInterface;

interface FilterInterface extends RequestDtoInterface
{

}
