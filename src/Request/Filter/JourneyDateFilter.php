<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Filter;

class JourneyDateFilter implements FilterInterface
{
    public function __construct(
        private ?SearchField $id,
        private ?SearchField $journeys,
        private ?SearchField $routes,
        private ?SearchField $carriers,
        private ?SearchField $transports,
        private ?SearchField $date,
        private ?SearchField $promo_data,
        private ?SearchField $is_sale_prohibited,
        private ?SearchField $is_boarding_finished,
        private ?SearchField $orders,
    ) {
    }

    public function getId(): ?SearchField
    {
        return $this->id;
    }

    public function getJourneys(): ?SearchField
    {
        return $this->journeys;
    }

    public function getRoutes(): ?SearchField
    {
        return $this->routes;
    }

    public function getCarriers(): ?SearchField
    {
        return $this->carriers;
    }

    public function getTransports(): ?SearchField
    {
        return $this->transports;
    }

    public function getDate(): ?SearchField
    {
        return $this->date;
    }

    public function getPromoData(): ?SearchField
    {
        return $this->promo_data;
    }

    public function getIsSaleProhibited(): ?SearchField
    {
        return $this->is_sale_prohibited;
    }

    public function getIsBoardingFinished(): ?SearchField
    {
        return $this->is_boarding_finished;
    }

    public function getOrders(): ?SearchField
    {
        return $this->orders;
    }
}
