<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Filter;

class CurrencyFilter implements FilterInterface
{
    public function __construct(
        private ?SearchField $id,
        private ?SearchField $iso,
    ) {
    }

    public function getId(): ?SearchField
    {
        return $this->id;
    }

    public function getIso(): ?SearchField
    {
        return $this->iso;
    }
}
