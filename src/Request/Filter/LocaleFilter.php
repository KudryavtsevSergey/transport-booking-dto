<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Filter;

class LocaleFilter implements FilterInterface
{
    public function __construct(
        private ?SearchField $code,
        private ?SearchField $country,
        private ?SearchField $name,
    ) {
    }

    public function getCode(): ?SearchField
    {
        return $this->code;
    }

    public function getCountry(): ?SearchField
    {
        return $this->country;
    }

    public function getName(): ?SearchField
    {
        return $this->name;
    }
}
