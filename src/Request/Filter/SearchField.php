<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Filter;

class SearchField
{
    public function __construct(
        private ?string $eq = null,
        private ?string $neq = null,
        private ?string $isNull = null,
        private ?string $notNull = null,
        private ?string $gt = null,
        private ?string $gte = null,
        private ?string $lt = null,
        private ?string $lte = null,
        private ?array $in = null,
        private ?array $notIn = null,
        private ?string $like = null,
    ) {
    }

    public function getEq(): ?string
    {
        return $this->eq;
    }

    public function getNeq(): ?string
    {
        return $this->neq;
    }

    public function getIsNull(): ?string
    {
        return $this->isNull;
    }

    public function getNotNull(): ?string
    {
        return $this->notNull;
    }

    public function getGt(): ?string
    {
        return $this->gt;
    }

    public function getGte(): ?string
    {
        return $this->gte;
    }

    public function getLt(): ?string
    {
        return $this->lt;
    }

    public function getLte(): ?string
    {
        return $this->lte;
    }

    public function getIn(): ?array
    {
        return $this->in;
    }

    public function getNotIn(): ?array
    {
        return $this->notIn;
    }

    public function getLike(): ?string
    {
        return $this->like;
    }
}
