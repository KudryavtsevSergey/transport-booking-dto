<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Filter;

class PassengerFilter implements FilterInterface
{
    public function __construct(
        private ?SearchField $id,
        private ?SearchField $name,
        private ?SearchField $surname,
        private ?SearchField $patronymic,
        private ?SearchField $document_number,
        private ?SearchField $gender,
        private ?SearchField $countries,
        private ?SearchField $document_types,
        private ?SearchField $customers,
        private ?SearchField $orders,
    ) {
    }

    public function getId(): ?SearchField
    {
        return $this->id;
    }

    public function getName(): ?SearchField
    {
        return $this->name;
    }

    public function getSurname(): ?SearchField
    {
        return $this->surname;
    }

    public function getPatronymic(): ?SearchField
    {
        return $this->patronymic;
    }

    public function getDocumentNumber(): ?SearchField
    {
        return $this->document_number;
    }

    public function getGender(): ?SearchField
    {
        return $this->gender;
    }

    public function getCountries(): ?SearchField
    {
        return $this->countries;
    }

    public function getDocumentTypes(): ?SearchField
    {
        return $this->document_types;
    }

    public function getCustomers(): ?SearchField
    {
        return $this->customers;
    }

    public function getOrders(): ?SearchField
    {
        return $this->orders;
    }
}
