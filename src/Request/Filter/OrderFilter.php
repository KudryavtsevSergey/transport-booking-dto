<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Filter;

class OrderFilter implements FilterInterface
{
    public function __construct(
        private ?SearchField $id,
        private ?SearchField $customers,
        private ?SearchField $currencies,
        private ?SearchField $paymentMethods,
        private ?SearchField $ticket,
        private ?SearchField $amount,
        private ?SearchField $statuses,
        private ?SearchField $isTicketsAutoCancelled,
        private ?SearchField $releaseIn,
    ) {
    }

    public function getId(): ?SearchField
    {
        return $this->id;
    }

    public function getCustomers(): ?SearchField
    {
        return $this->customers;
    }

    public function getCurrencies(): ?SearchField
    {
        return $this->currencies;
    }

    public function getPaymentMethods(): ?SearchField
    {
        return $this->paymentMethods;
    }

    public function getTicket(): ?SearchField
    {
        return $this->ticket;
    }

    public function getAmount(): ?SearchField
    {
        return $this->amount;
    }

    public function getStatuses(): ?SearchField
    {
        return $this->statuses;
    }

    public function getIsTicketsAutoCancelled(): ?SearchField
    {
        return $this->isTicketsAutoCancelled;
    }

    public function getReleaseIn(): ?SearchField
    {
        return $this->releaseIn;
    }
}
