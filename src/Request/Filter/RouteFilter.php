<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Filter;

class RouteFilter implements FilterInterface
{
    public function __construct(
        private ?SearchField $id,
        private ?SearchField $name,
    ) {
    }

    public function getId(): ?SearchField
    {
        return $this->id;
    }

    public function getName(): ?SearchField
    {
        return $this->name;
    }
}
