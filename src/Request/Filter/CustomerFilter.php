<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Filter;

class CustomerFilter implements FilterInterface
{
    public function __construct(
        private ?SearchField $id,
        private ?SearchField $name,
        private ?SearchField $surname,
        private ?SearchField $patronymic,
        private ?SearchField $email,
    ) {
    }

    public function getId(): ?SearchField
    {
        return $this->id;
    }

    public function getName(): ?SearchField
    {
        return $this->name;
    }

    public function getSurname(): ?SearchField
    {
        return $this->surname;
    }

    public function getPatronymic(): ?SearchField
    {
        return $this->patronymic;
    }

    public function getEmail(): ?SearchField
    {
        return $this->email;
    }
}
