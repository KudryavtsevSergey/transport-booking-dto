<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Filter;

class CountryFilter implements FilterInterface
{
    public function __construct(
        private ?SearchField $id,
        private ?SearchField $alpha2,
        private ?SearchField $phoneCode,
        private ?SearchField $name,
    ) {
    }

    public function getId(): ?SearchField
    {
        return $this->id;
    }

    public function getAlpha2(): ?SearchField
    {
        return $this->alpha2;
    }

    public function getPhoneCode(): ?SearchField
    {
        return $this->phoneCode;
    }

    public function getName(): ?SearchField
    {
        return $this->name;
    }
}
