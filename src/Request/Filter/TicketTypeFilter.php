<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Filter;

class TicketTypeFilter implements FilterInterface
{
    public function __construct(
        private ?SearchField $id,
        private ?SearchField $name,
        private ?SearchField $code,
        private ?SearchField $isWithoutSeat,
    ) {
    }

    public function getId(): ?SearchField
    {
        return $this->id;
    }

    public function getName(): ?SearchField
    {
        return $this->name;
    }

    public function getCode(): ?SearchField
    {
        return $this->code;
    }

    public function getIsWithoutSeat(): ?SearchField
    {
        return $this->isWithoutSeat;
    }
}
