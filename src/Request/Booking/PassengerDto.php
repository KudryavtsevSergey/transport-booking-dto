<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Booking;

class PassengerDto
{
    public function __construct(
        private ?int $id,
        private string $surname,
        private string $name,
        private ?string $patronymic,
        private string $gender,
        private string $documentNumber,
        private int $documentTypeId,
    ) {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    public function getGender(): string
    {
        return $this->gender;
    }

    public function getDocumentNumber(): string
    {
        return $this->documentNumber;
    }

    public function getDocumentTypeId(): int
    {
        return $this->documentTypeId;
    }
}
