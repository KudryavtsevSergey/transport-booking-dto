<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Booking;

use Sun\TransportBookingDto\Request\BodyDtoInterface;

class CustomerRequestDto implements BodyDtoInterface
{
    /**
     * @param string $name
     * @param string $surname
     * @param string|null $patronymic
     * @param string $email
     * @param PhoneDto[] $phones
     */
    public function __construct(
        private string $name,
        private string $surname,
        private ?string $patronymic,
        private string $email,
        private array $phones,
    ) {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPhones(): array
    {
        return $this->phones;
    }
}
