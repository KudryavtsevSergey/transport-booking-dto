<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Booking;

class RegisterCustomerRequestDto extends CustomerRequestDto
{
    /**
     * @param string $name
     * @param string $surname
     * @param string|null $patronymic
     * @param string $email
     * @param PhoneDto[] $phones
     * @param string|null $password
     * @param string|null $passwordConfirmation
     */
    public function __construct(
        string $name,
        string $surname,
        ?string $patronymic,
        string $email,
        array $phones,
        private ?string $password = null,
        private ?string $passwordConfirmation = null,
    ) {
        parent::__construct(
            $name,
            $surname,
            $patronymic,
            $email,
            $phones
        );
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getPasswordConfirmation(): ?string
    {
        return $this->passwordConfirmation;
    }
}
