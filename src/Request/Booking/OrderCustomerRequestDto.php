<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Booking;

class OrderCustomerRequestDto extends CustomerRequestDto
{
    /**
     * @param int|null $id
     * @param string $name
     * @param string $surname
     * @param string|null $patronymic
     * @param string $email
     * @param PhoneDto[] $phones
     */
    public function __construct(
        private ?int $id,
        string $name,
        string $surname,
        ?string $patronymic,
        string $email,
        array $phones,
    ) {
        parent::__construct(
            $name,
            $surname,
            $patronymic,
            $email,
            $phones
        );
    }

    public function getId(): ?int
    {
        return $this->id;
    }
}
