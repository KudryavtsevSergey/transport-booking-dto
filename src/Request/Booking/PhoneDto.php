<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Booking;

class PhoneDto
{
    /**
     * @param int|null $id
     * @param string $number
     * @param int $countryId
     * @param bool $useForNotifications
     * @param int[] $socialProviders
     */
    public function __construct(
        private ?int $id,
        private string $number,
        private int $countryId,
        private bool $useForNotifications = false,
        private array $socialProviders = [],
    ) {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function getCountryId(): int
    {
        return $this->countryId;
    }

    public function isUseForNotifications(): bool
    {
        return $this->useForNotifications;
    }

    public function getSocialProviders(): array
    {
        return $this->socialProviders;
    }
}
