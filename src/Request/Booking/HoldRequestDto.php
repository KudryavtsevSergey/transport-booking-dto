<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Booking;

use Sun\TransportBookingDto\Request\BodyDtoInterface;

class HoldRequestDto implements BodyDtoInterface
{
    /**
     * @param int $currencyId
     * @param HoldTicketTypeDto[] $ticketTypes
     * @param JourneyHoldDto[] $journeyHolds
     */
    public function __construct(
        private int $currencyId,
        private array $ticketTypes,
        private array $journeyHolds,
    ) {
    }

    public function getCurrencyId(): int
    {
        return $this->currencyId;
    }

    public function getTicketTypes(): array
    {
        return $this->ticketTypes;
    }

    public function getJourneyHolds(): array
    {
        return $this->journeyHolds;
    }
}
