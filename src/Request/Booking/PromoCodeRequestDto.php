<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Booking;

use Sun\TransportBookingDto\Request\BodyDtoInterface;

class PromoCodeRequestDto implements BodyDtoInterface
{
    public function __construct(
        private string $promoCode,
    ) {
    }

    public function getPromoCode(): string
    {
        return $this->promoCode;
    }
}
