<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Booking;

class OrderPassengerDto
{
    /**
     * @param int $ticketTypeId
     * @param PassengerDto $passenger
     * @param OrderPassengerBaggageDto[] $baggage
     * @param OrderPassengerTicketServiceDto[] $ticketServices
     */
    public function __construct(
        private int $ticketTypeId,
        private PassengerDto $passenger,
        private array $baggage = [],
        private array $ticketServices = [],
    ) {
    }

    public function getTicketTypeId(): ?int
    {
        return $this->ticketTypeId;
    }

    public function getPassenger(): PassengerDto
    {
        return $this->passenger;
    }

    public function getBaggage(): array
    {
        return $this->baggage;
    }

    public function getTicketServices(): array
    {
        return $this->ticketServices;
    }
}
