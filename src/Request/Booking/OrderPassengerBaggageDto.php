<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Booking;

class OrderPassengerBaggageDto
{
    public function __construct(
        private int $journeyDateId,
        private int $baggageTypeId,
        private int $numberOfBaggage,
    ) {
    }

    public function getJourneyDateId(): int
    {
        return $this->journeyDateId;
    }

    public function getBaggageTypeId(): int
    {
        return $this->baggageTypeId;
    }

    public function getNumberOfBaggage(): int
    {
        return $this->numberOfBaggage;
    }
}
