<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Booking;

class JourneyHoldDto
{
    /**
     * @param int $journeyDateId
     * @param int $fromStopId
     * @param int $toStopId
     * @param int $seatTypeId
     * @param int[] $seats
     */
    public function __construct(
        private int $journeyDateId,
        private int $fromStopId,
        private int $toStopId,
        private int $seatTypeId,
        private array $seats = [],
    ) {
    }

    public function getJourneyDateId(): int
    {
        return $this->journeyDateId;
    }

    public function getFromStopId(): int
    {
        return $this->fromStopId;
    }

    public function getToStopId(): int
    {
        return $this->toStopId;
    }

    public function getSeatTypeId(): int
    {
        return $this->seatTypeId;
    }

    public function getSeats(): array
    {
        return $this->seats;
    }
}
