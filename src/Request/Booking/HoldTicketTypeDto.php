<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Booking;

class HoldTicketTypeDto
{
    public function __construct(
        private int $ticketTypeId,
        private int $count,
    ) {
    }

    public function getTicketTypeId(): int
    {
        return $this->ticketTypeId;
    }

    public function getCount(): int
    {
        return $this->count;
    }
}
