<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Booking;

class OrderPassengerTicketServiceDto
{
    public function __construct(
        private int $journeyDateId,
        private int $paidServiceId,
    ) {
    }

    public function getJourneyDateId(): int
    {
        return $this->journeyDateId;
    }

    public function getPaidServiceId(): int
    {
        return $this->paidServiceId;
    }
}
