<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Booking;

use Sun\TransportBookingDto\Request\QueryDtoInterface;

class OffersRequestDto implements QueryDtoInterface
{
    public function __construct(
        private string $forwardDate,
        private ?string $backDate = null,
    ) {
    }

    public function getForwardDate(): string
    {
        return $this->forwardDate;
    }

    public function getBackDate(): ?string
    {
        return $this->backDate;
    }
}
