<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request\Booking;

use Sun\TransportBookingDto\Request\BodyDtoInterface;

class OrderRequestDto implements BodyDtoInterface
{
    /**
     * @param int $paymentMethodId
     * @param OrderPassengerDto[] $passengers
     * @param OrderCustomerRequestDto $customer
     * @param string|null $note
     * @param int[] $notificationProviders
     * @param OrderJourneyDateTransferDto[] $orderJourneyDateTransfers
     */
    public function __construct(
        private int $paymentMethodId,
        private array $passengers,
        private OrderCustomerRequestDto $customer,
        private ?string $note,
        private array $notificationProviders,
        private array $orderJourneyDateTransfers = [],
    ) {
    }

    public function getPaymentMethodId(): int
    {
        return $this->paymentMethodId;
    }

    public function getPassengers(): array
    {
        return $this->passengers;
    }

    public function getCustomer(): OrderCustomerRequestDto
    {
        return $this->customer;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function getNotificationProviders(): array
    {
        return $this->notificationProviders;
    }

    public function getOrderJourneyDateTransfers(): array
    {
        return $this->orderJourneyDateTransfers;
    }
}
