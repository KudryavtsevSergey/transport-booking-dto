<?php

declare(strict_types=1);

namespace Sun\TransportBookingDto\Request;

use Sun\TransportBookingDto\DtoInterface;

interface RequestDtoInterface extends DtoInterface
{

}
